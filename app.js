var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// ================================================
var noble = require('noble');
// ================================================


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var devicelistRouter = require('./routes/devicelist');
var deviceconnectRouter = require('./routes/deviceconnect');
var devicediscoverRouter = require('./routes/devicediscover');
var deviceresourceRouter = require('./routes/deviceresource');


var app = express();



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// ======================================= BLE SERVICE ================================================
// ====================================================================================================

function DEBUG() {
	console.log.apply(this, arguments);
}



var BLEDeviceList = [];
var connectedDevice = { isConnected: 'false',
												device: null,
												services: null,
												isCharacteristicsInit: false
											};

var serviceDataInfo = { stepCount: null };


// Filter for BLE Device with long period of not advertising
function filterForBLEDeviceList()
{
	var timestamp = Date.now();

	BLEDeviceList = BLEDeviceList.filter(function(BLEDevice) {
		return (timestamp - BLEDevice.advertTimestamp) < 2000;
	});

	DEBUG("filterForBLEDeviceList(): BLEDeviceList.length: ", BLEDeviceList.length);
}


// Cleanup BLEDeviceList periodic function
var BLEDeviceListCleanupTimer = setInterval(filterForBLEDeviceList, 500);



noble.on('stateChange', function(state)
{
		DEBUG("noble => stateChanged: ", state);

		if (state === 'poweredOn')
		{
			DEBUG("noble => startScanning()");
    	noble.startScanning([], true, function(error) {});
  	}
		else
		{
			DEBUG("noble => stopScanning()");
    	noble.stopScanning();
  	}
});




noble.on('discover', function(peripheral)
{
		DEBUG("noble => discover");

		var BLEDevice = BLEDeviceList.find(function(device) { return device.BLEDeviceInfo.id === peripheral.id; });

		if(BLEDevice === undefined || BLEDevice === null)
		{
			var newBLEDevice = { BLEDeviceInfo: peripheral, advertTimestamp: Date.now() };

			DEBUG("newDevice.BLEDeviceInfo.id: ", newBLEDevice.BLEDeviceInfo.id);
			DEBUG("newDevice.timestamp: ", newBLEDevice.advertTimestamp);

			BLEDeviceList.push(newBLEDevice);
		}
		else
		{
			DEBUG("device.BLEDeviceInfo.id: ", BLEDevice.BLEDeviceInfo.id);
			DEBUG("device.timestamp: ", BLEDevice.advertTimestamp);

			BLEDevice.advertTimestamp = Date.now();
		}
});


// ====================================================================================================
// ====================================================================================================

app.use(function(req,res,next) {
	req.noble = noble;
	req.BLEDeviceList = BLEDeviceList;
	req.connectedDevice = connectedDevice;
	req.serviceDataInfo = serviceDataInfo;

	//console.log("app.use(): DeviceList: ", deviceList);
	console.log("INIT NOBLE");

	next();
});


// ================================================

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/device/list', devicelistRouter);
app.use('/device/connect', deviceconnectRouter);
app.use('/device/discover', devicediscoverRouter);
app.use('/device', deviceresourceRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
