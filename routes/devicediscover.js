var express = require('express');
var router = express.Router();



/* Discover BLE services */
router.get('/service', function(req, res, next) {
	var noble = req.noble;
	var BLEDeviceList = req.BLEDeviceList;
	var	connectedDevice = req.connectedDevice;
	var device = null;

	console.log("========================================================================");
	console.log("connectedDevice:");
	console.log(connectedDevice);
	console.log("========================================================================");

	// Check is APP already connected to any BLE device
	if(connectedDevice.isConnected !== true)
	{
		console.log("isConnected: " + connectedDevice.isConnected);
		res.status(400).send('You are not connected to any device');
		return;
	}

	device = connectedDevice.device;

	if(connectedDevice.services === null)
	{
  		device.discoverServices([], function(error, services)
			{
					console.log(services);

					// Init connected devices services object
					connectedDevice.services = [];

					for(var idx = 0; idx < services.length; ++idx)
					{
						var serviceObject	= {
																	serviceInfo:			services[idx],
																	characteristics: 	null
																};

						connectedDevice.services.push(serviceObject);
					}


					// Generate BLE services view list and send response
					var BLEServiceViewList = [];

					connectedDevice.services.forEach(function(service){

							var BLEServiceView = { serviceName: 	  service.serviceInfo.name,
														   	     serviceUUID:  	  service.serviceInfo.uuid,
													     	     serviceType:  		service.serviceInfo.type
														 	 		 };

							BLEServiceViewList.push(BLEServiceView);

					});

					res.json(BLEServiceViewList);

			});
	}
	else
	{
			// Generate BLE services view list and send response
			var BLEServiceViewList = [];

			connectedDevice.services.forEach(function(service)
			{
					var BLEServiceView = { serviceName: 	  service.serviceInfo.name,
																 serviceUUID:  	  service.serviceInfo.uuid,
																 serviceType:  		service.serviceInfo.type
															 };

					BLEServiceViewList.push(BLEServiceView);
			});

  		res.json(BLEServiceViewList);
	}


});





/* Discover BLE characteristics */
router.get('/characteristic', function(req, res, next) {
	var noble = req.noble;
	var BLEDeviceList = req.BLEDeviceList;
	var	connectedDevice = req.connectedDevice;
	var device = null;

	console.log("========================================================================");
	console.log("connectedDevice:");
	console.log(connectedDevice);
	console.log("========================================================================");


	// Check is APP already connected to any BLE device
	if(connectedDevice.isConnected !== true)
	{
		console.log("isConnected: " + connectedDevice.isConnected);
		res.status(400).send('You are not connected to any device');
		return;
	}

	device = connectedDevice.device;

	if(connectedDevice.isCharacteristicsInit !== true)
	{

			// Can not discover characteristic of services if services is not discovered
			if(connectedDevice.services === null)
			{
					res.status(400).send('You are not discover services');
					return;
			}


			var discoveredServicesCount = 0;

			// Discover characteristics of all services
			for(var idx = 0; idx < connectedDevice.services.length; ++idx)
			{

					var discoverCharacteristic = function(){

					var idx_cpy = idx;

					connectedDevice.services[idx_cpy].serviceInfo.discoverCharacteristics([], function(error, characteristics)
					{
							console.log('connectedDevice.services[idx]: ');

							console.log('connectedDevice.services.length: ');
							console.log(connectedDevice.services.length);


							console.log('--------------------------------------------------------------');
							console.log('--------------------------------------------------------------');
							console.log("idx: ", idx_cpy);
							console.log('characteristics: ');
							console.log(characteristics);
							console.log('--------------------------------------------------------------');
							console.log('--------------------------------------------------------------');


							// Add characteristics to service
							connectedDevice.services[idx_cpy].characteristics = characteristics;

							// If characteristics for the last service is discovered, then send response
							++discoveredServicesCount;
							if(discoveredServicesCount === connectedDevice.services.length)
							{
									connectedDevice.isCharacteristicsInit = true;

									var BLECharacteristicViewList = [];

									for(var i = 0; i < connectedDevice.services.length; ++i)
									{
										if(connectedDevice.services[i].characteristics === null)
											continue;

										for(var j = 0; j < connectedDevice.services[i].characteristics.length; ++j)
										{

												var BLECharacteristicView = { serviceUUID: 				 			connectedDevice.services[i].serviceInfo.uuid,
													     	     									characteristicName:	 			connectedDevice.services[i].characteristics[j].name,
																											characteristicUUID:  			connectedDevice.services[i].characteristics[j].uuid,
													     	     									characteristicType:	 			connectedDevice.services[i].characteristics[j].type,
																											characteristicProperties: connectedDevice.services[i].characteristics[j].properties
																										};

												BLECharacteristicViewList.push(BLECharacteristicView);
										}
									}

									res.json(BLECharacteristicViewList);
							}
					});

					}();

			}
	}
	else
	{
			var BLECharacteristicViewList = [];

			for(var i = 0; i < connectedDevice.services.length; ++i)
			{
					if(connectedDevice.services[i].characteristics === null)
						continue;

					for(var j = 0; j < connectedDevice.services[i].characteristics.length; ++j)
					{
							var BLECharacteristicView = { serviceUUID: 				 			connectedDevice.services[i].serviceInfo.uuid,
													     	     				characteristicName:	 			connectedDevice.services[i].characteristics[j].name,
																						characteristicUUID:  			connectedDevice.services[i].characteristics[j].uuid,
													     	     				characteristicType:	 			connectedDevice.services[i].characteristics[j].type,
																						characteristicProperties: connectedDevice.services[i].characteristics[j].properties
																					};

							BLECharacteristicViewList.push(BLECharacteristicView);
					}
			}

			res.json(BLECharacteristicViewList);
	}


});











module.exports = router;
