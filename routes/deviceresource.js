
var express = require('express');
var fs = require('fs');
var router = express.Router();

var multer = require('multer');
var upload = multer({
	limits: {
		fileSize: 4 * 400 * 400
	}
});


var sharp = require('sharp');



/* Change device LED state */
router.get('/LED/:LEDState', function(req, res, next) {
	var noble = req.noble;
	var LEDState = req.params.LEDState;
	var	connectedDevice = req.connectedDevice;


	if(connectedDevice.isConnected !== true)
	{
			res.status(400).send('You are not connected to any device');
			return;
	}

	if(connectedDevice.services === null)
	{
			res.status(400).send('You are not discover services');
			return;
	}

	if(connectedDevice.isCharacteristicsInit !== true)
	{
			res.status(400).send('Characteristics are not discovered');
			return;
	}


	for(var i = 0; i < connectedDevice.services.length; ++i)
	{
		if(connectedDevice.services[i].characteristics === null)
				continue;

		for(var j = 0; j < connectedDevice.services[i].characteristics.length; ++j)
		{
			var serviceUUID 			 = connectedDevice.services[i].serviceInfo.uuid;
			var characteristicUUID = connectedDevice.services[i].characteristics[j].uuid;

			if(serviceUUID === "f364140000b04240ba5005ca45bf8abc" && characteristicUUID === "f364140100b04240ba5005ca45bf8abc")
			{
					var newLEDState = new Buffer(1);

					if(LEDState === "on")
							newLEDState.writeUInt8(1, 0);
					else
							newLEDState.writeUInt8(0, 0);

					connectedDevice.services[i].characteristics[j].write(newLEDState, false, function(err)
					{
          		if (err)
							{
									console.log('Change LEDState error');
									res.status(400).send('Error while changing LED state');
									return;
							}

							res.status(200).send('OK');
							return;
					});

			}

		}
	}

});










function initBLETransfer(connectedDevice, data, info)
{

	console.log("initBLETransfer(): called");

	for(var i = 0; i < connectedDevice.services.length; ++i)
	{
		if(connectedDevice.services[i].characteristics === null)
				continue;

		for(var j = 0; j < connectedDevice.services[i].characteristics.length; ++j)
		{
			var serviceUUID 			 = connectedDevice.services[i].serviceInfo.uuid;
			var characteristicUUID = connectedDevice.services[i].characteristics[j].uuid;

			if(serviceUUID === "41d51500d21c11e9bb652a2ae2dbcce4" && characteristicUUID === "41d51503d21c11e9bb652a2ae2dbcce4")
			{

					console.log("initBLETransfer(): recognized service");


					var bufferPos = 0;
					var sendDumbPacket = 0;

					for( ; bufferPos < data.length; )
					{

						console.log("initBLETransfer(): data length: " + data.length);

						console.log("initBLETransfer(): bufferPos " + bufferPos);

						var writeLength = 17;

						if(data.length - bufferPos < 17)
							writeLength = data.length - bufferPos;

						if(data.length - bufferPos == 17)
							sendDumbPacket = 1;


						var buffer = new Buffer(writeLength + 3);
						console.log("initBLETransfer(): buffer allocated");


						buffer.writeUInt8(writeLength, 0);
						console.log("initBLETransfer(): msg len written");

						buffer.writeUInt16LE(bufferPos/17, 1);
						console.log("initBLETransfer(): msg pos written");

						//var msg = Buffer.from(data, bufferPos, writeLength);
						//msg.copy(buffer, 3, 0, msg.length);
						data.copy(buffer, 3, bufferPos, bufferPos + writeLength);

						console.log("initBLETransfer(): msg written");


						bufferPos += writeLength;
						console.log("initBLETransfer(): new bufferPos: " + bufferPos);

						console.log("initBLETransfer(): buffer: ");
						console.log(buffer);

						connectedDevice.services[i].characteristics[j].write(buffer, false, function(err)
						{
          			if(err)
								{
									console.log('ERROR IN SENDING IMAGE');
									return;
								}

								//console.log('IMAGE MODUL SENT');

						});

					}

					if(sendDumbPacket == 1)
					{
						var bufferDumbPacket = new Buffer(writeLength + 3);
						console.log("initBLETransfer(): buffer allocated");


						bufferDumbPacket.writeUInt8(0, 0);
						console.log("initBLETransfer(): msg len written");

						bufferDumbPacket.writeUInt16LE(bufferPos/17, 1);
						console.log("initBLETransfer(): msg pos written");

						connectedDevice.services[i].characteristics[j].write(bufferDumbPacket, false, function(err)
						{
          			if(err)
								{
									console.log('ERROR IN SENDING DUMB PACKET');
									return;
								}

								//console.log('IMAGE MODUL SENT');
						});

					}
					break;
			}

		}
	}


	/*
	console.log("initBLETrasnfer(): data => ");
	console.log(data);

	console.log("initBLETrasnfer(): info => ");
	console.log(info);

	sharp(data)
		.toFile('output.jpeg')
		.then(info => { console.log("Successull image saving"); })
		.catch(info	=> { console.log("Error in image saving"); });
	*/
}





/* Upload image on device */
router.post('/image', upload.single('image'), async function(req, res)
{

	var noble = req.noble;
	var	connectedDevice = req.connectedDevice;



	if(connectedDevice.isConnected !== true)
	{
			res.status(400).send('You are not connected to any device');
			return;
	}


	if(connectedDevice.services === null)
	{
			res.status(400).send('You are not discover services');
			return;
	}


	if(connectedDevice.isCharacteristicsInit !== true)
	{
			res.status(400).send('Characteristics are not discovered');
			return;
	}


	if(!req.file)
	{
		res.status(400).send('Image is not sent');
		console.log("Image is not uploaded!");
		return;
	}




	console.log("req.file.buffer: ");
	console.log(req.file.buffer);

	var isErrorHappened = false;

	await sharp(req.file.buffer)
						.resize(390, 390)
						.jpeg({
								quality: 75
								// chromaSubsampling: '4:4:4'
						})
						.toBuffer({ resolveWithObject: true })
						.then(({data, info}) => {

								//initBLETransfer(connectedDevice, data, info);


								console.log("data.length:");
								console.log(data.length);

								console.log("info");
								console.log(info);

								sharp(data).toFile('output1.jpg').then(function(){
										var img = fs.readFileSync('output1.jpg');
										console.log('fs.readFileSync(): rv => ');
										console.log(img);
										console.log("img.length: ");
										console.log(img.length);
										initBLETransfer(connectedDevice, img, info);
								});

						})
						.catch(err => {
								console.log("Error happened in coverting and sending file");
								console.log("Error: ");
								console.log(err);
								isErrorHappened = true;
								res.status(400).send('Error: ' + err);
								return;
						});

	if(!isErrorHappened)
	{
		console.log("Image is uploaded!");
		res.status(200).send('OK');
	}

});













/* Change device LED state */
router.get('/clocksync/:year/:month/:day/:wdu/:hour/:minute/:second', function(req, res, next) {

	console.log("clocksync called");

	var noble = req.noble;
	var	connectedDevice = req.connectedDevice;
	var BLEservice = null;

	var startedSTransaction = 0;
	var compleatedTransaction = 0;
	var isErrorHappened = false;

	var year 	 = req.params.year;
	var month  = req.params.month;
	var day 	 = req.params.day;
	var wdu 	 = req.params.wdu;
	var hour	 = req.params.hour;
	var minute = req.params.minute;
	var second = req.params.second;



	if(connectedDevice.isConnected !== true)
	{
			res.status(400).send('You are not connected to any device');
			return;
	}

	if(connectedDevice.services === null)
	{
			res.status(400).send('You are not discover services');
			return;
	}

	if(connectedDevice.isCharacteristicsInit !== true)
	{
			res.status(400).send('Characteristics are not discovered');
			return;
	}


	console.log("clocksync check passed");


	for(var i = 0; i < connectedDevice.services.length; ++i)
	{
			var serviceUUID = connectedDevice.services[i].serviceInfo.uuid;

			if(serviceUUID === "86e91000d7984938a5f51cc715fbdde1")
			{
					BLEservice = connectedDevice.services[i];
					break;
			}
	}

	console.log("clocksync find service");


	if(BLEservice === null && BLEservice.characteristics === null)
	{
			return;
	}



	for(var j = 0; j < BLEservice.characteristics.length; ++j)
	{
			var characteristicUUID = BLEservice.characteristics[j].uuid;



			// ===========================> YEAR <=============================
			if(characteristicUUID === "86e91001d7984938a5f51cc715fbdde1")
			{

					//console.log("clocksync send YEAR req for update");

					year = parseInt(year);
					year -= 2000;

					try
					{
						var newYear = new Buffer(1);
						newYear.writeUInt8(year, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send YEAR - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;

					BLEservice.characteristics[j].write(newYear, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{

									isErrorHappened = true;
									console.log('Update year error');
									res.status(400).send('Error while uploading year');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}



			// ===========================> MONTH <=============================
			if(characteristicUUID === "86e91002d7984938a5f51cc715fbdde1")
			{

					//console.log("clocksync send MONTH req for update");

					month = parseInt(month);

					try
					{
						var newMonth = new Buffer(1);
						newMonth.writeUInt8(month, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send MONTH - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;

					BLEservice.characteristics[j].write(newMonth, false, function(err)
					{
							console.log("Month update complated");
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update month error');
									res.status(400).send('Error while uploading month');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}



			// ===========================> DAY <=============================
			if(characteristicUUID === "86e91003d7984938a5f51cc715fbdde1")
			{

					//console.log("clocksync send DAY req for update");

					day = parseInt(day);

					try
					{
						var newDay = new Buffer(1);
						newDay.writeUInt8(day, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send DAY - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;


					BLEservice.characteristics[j].write(newDay, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update day error');
									res.status(400).send('Error while uploading day');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}



			// ===========================> WDU <=============================
			if(characteristicUUID === "86e91004d7984938a5f51cc715fbdde1")
			{

					//console.log("clocksync send WDU req for update");

					wdu = parseInt(wdu);

					try
					{
						var newWDU = new Buffer(1);
						newWDU.writeUInt8(wdu, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send WDU - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;


					BLEservice.characteristics[j].write(newWDU, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update WDU error');
									res.status(400).send('Error while uploading WDU');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}




			// ===========================> HOUR <=============================
			if(characteristicUUID === "86e91005d7984938a5f51cc715fbdde1")
			{
					//console.log("clocksync send HOUR req for update");

					hour = parseInt(hour);

					try
					{
						var newHour = new Buffer(1);
						newHour.writeUInt8(hour, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send HOUR - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;

					BLEservice.characteristics[j].write(newHour, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update hour error');
									res.status(400).send('Error while uploading hour');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}




			// ===========================> MINUTE <=============================
			if(characteristicUUID === "86e91006d7984938a5f51cc715fbdde1")
			{
					//console.log("clocksync send MINUTE req for update");

					minute = parseInt(minute);

					try
					{
						var newMinute = new Buffer(1);
						newMinute.writeUInt8(minute, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send MINUTE - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;


					BLEservice.characteristics[j].write(newMinute, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update minute error');
									res.status(400).send('Error while uploading minute');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}




			// ===========================> SECOND <=============================
			if(characteristicUUID === "86e91007d7984938a5f51cc715fbdde1")
			{

					//console.log("clocksync send SECOND req for update");

					second = parseInt(second);

					try
					{
						var newSecond = new Buffer(1);
						newSecond.writeUInt8(second, 0);
					}
					catch(err)
					{
						console.log(err);
						return;
					}

					console.log("clocksync send SECOND - before write => " + j);

					//while(compleatedTransaction != startedSTransaction);
					++startedSTransaction;


					BLEservice.characteristics[j].write(newSecond, false, function(err)
					{
							++compleatedTransaction;
							if (err)
							{
									isErrorHappened = true;
									console.log('Update second error');
									res.status(400).send('Error while uploading second');
									//return;
							}

							//res.status(200).send('OK');
							//return;
					});
			}

	}


	console.log("clocksync send req for update");


//	while(compleatedTransaction != 7 && !isErrorHappened);

//	if(isErrorHappened)
//	{
//		return;
//	}




	for(var j = 0; j < BLEservice.characteristics.length; ++j)
	{
			var characteristicUUID = BLEservice.characteristics[j].uuid;

			// ===========================> UPLOAD <=============================
			if(characteristicUUID === "86e91008d7984938a5f51cc715fbdde1")
			{
					var upload = new Buffer(1);
					upload.writeUInt8(1, 0);

					BLEservice.characteristics[j].write(upload, false, function(err)
					{
          		if (err)
							{
									console.log('Update year error');
									res.status(400).send('Error while sending update request');
									return;
							}

							res.status(200).send('OK');
							return;
					});
			}
	}


});














router.get('/stepCount/register', function(req, res, next) {
	var noble = req.noble;
	var LEDState = req.params.LEDState;
	var	connectedDevice = req.connectedDevice;
	var serviceDataInfo = req.serviceDataInfo


	if(connectedDevice.isConnected !== true)
	{
			res.status(400).send('You are not connected to any device');
			return;
	}

	if(connectedDevice.services === null)
	{
			res.status(400).send('You are not discover services');
			return;
	}

	if(connectedDevice.isCharacteristicsInit !== true)
	{
			res.status(400).send('Characteristics are not discovered');
			return;
	}


	for(var i = 0; i < connectedDevice.services.length; ++i)
	{
		if(connectedDevice.services[i].characteristics === null)
				continue;

		for(var j = 0; j < connectedDevice.services[i].characteristics.length; ++j)
		{
			var serviceUUID 			 = connectedDevice.services[i].serviceInfo.uuid;
			var characteristicUUID = connectedDevice.services[i].characteristics[j].uuid;

			if(serviceUUID === "c1be1700cb954761987662c41cfca01a" && characteristicUUID === "c1be1701cb954761987662c41cfca01a")
			{
					console.log("STEP COUNT characteristic finded");

					connectedDevice.services[i].characteristics[j].subscribe(function(err)
					{
							if(err !== undefined && err !== null)
							{
									console.log("stepCount register ERROR");
							}
					});

					connectedDevice.services[i].characteristics[j].on('read', function(data, isNotification)
					{
							if(data.length === 4)
							{
									var result = data.readUInt32LE(0);
									serviceDataInfo.stepCount = result;
									console.log("stepCount: ", result);
							}
							else
							{
									console.log("stepCount read ERROR, data.length: ", data.length);
							}
					});

			}

		}
	}

});



router.get('/stepCount', function(req, res, next) {
	var noble = req.noble;
	var LEDState = req.params.LEDState;
	var	connectedDevice = req.connectedDevice;
	var serviceDataInfo = req.serviceDataInfo

	var stepCount = serviceDataInfo.stepCount;

	if(stepCount !== null)
		stepCount = stepCount.toString();
	else
		stepCount = "Unknown";

	res.status(200).send(stepCount);
});




















module.exports = router;
