var express = require('express');
var router = express.Router();

/* Connect to BLE device */
router.get('/:deviceAddress', function(req, res, next) {
	var noble = req.noble;
	var BLEDeviceList = req.BLEDeviceList;
	var	connectedDevice = req.connectedDevice;
	var deviceAddress = req.params.deviceAddress;
	var deviceToConnect = null;

	//console.log("connectedDevice:");
	//console.log(connectedDevice);

	// Check is APP already connected to some BLE device
	if(connectedDevice.isConnected === true)
	{
		res.status(409).send('You are already connected to the device with address ' + connectedDevice.device.address);
		return;
	}


	// Check does device with given address is advertising
	// (This is not the most performant way to find device, but work)
	BLEDeviceList.forEach(function(device){
		if(device.BLEDeviceInfo.address === deviceAddress)
			deviceToConnect = device.BLEDeviceInfo;
	});

	if(deviceToConnect === null)
	{
		res.status(404).send('Device with the address ' + deviceAddress + ' is not found');
		return;
	}



	deviceToConnect.on('disconnect', function(){
			connectedDevice.isConnected = false;
			connectedDevice.device = null;
			connectedDevice.services = null;
			connectedDevice.isCharacteristicsInit = false;
	});

	deviceToConnect.connect(function(error){
			connectedDevice.isConnected = true;
			connectedDevice.device = deviceToConnect;
			console.log("Connect to device");
			//res.sendStatus(200);
			//res.status(302).('http://localhost:3000/main');
			res.redirect(302, 'http://localhost:3000/main');
	});


});


module.exports = router;
