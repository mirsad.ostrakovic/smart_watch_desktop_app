var express = require('express');
var router = express.Router();

/* GET home page */
router.get('/', function(req, res, next) {
  res.render('devicelist', {});
});


/* GET DeviceList page */
router.get('/devicelist', function(req, res, next) {
  res.render('devicelist', {});
});


/* GET main page */
router.get('/main', function(req, res, next) {
  res.render('smartwatch', {});
});

/* GET SmartWatch page */
router.get('/smartwatch', function(req, res, next) {
  res.render('smartwatch', {});
});




/* GET UploadImage page */
router.get('/uploadimage', function(req, res, next) {
  res.render('uploadimage', {});
});


module.exports = router;
