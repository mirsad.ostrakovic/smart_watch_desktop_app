var express = require('express');
var router = express.Router();

/* GET BLE device list */
router.get('/', function(req, res, next) {
	var noble = req.noble;
	var BLEDeviceList = req.BLEDeviceList;
	var	connectedDevice = req.connectedDevice;


	var BLEDeviceListView = [];

	BLEDeviceList.forEach(function(device){

		console.log("Device:");
		console.log(device);

		var BLEDeviceView = { deviceName: 		device.BLEDeviceInfo.advertisement.localName,
													deviceAddress:	device.BLEDeviceInfo.address,
													deviceUUID:  		device.BLEDeviceInfo.uuid,
													isConnectable:  device.BLEDeviceInfo.connectable,
													rssi:						device.BLEDeviceInfo.rssi
												};

		BLEDeviceListView.push(BLEDeviceView);
	});

	console.log("DeviceList: ");
	console.log(BLEDeviceListView);
	console.log("Before send device/list");
  res.json(BLEDeviceListView);
	console.log("After send device/list");
});


module.exports = router;
