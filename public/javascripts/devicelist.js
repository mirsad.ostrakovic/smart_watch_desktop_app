console.log("devicelist.js loaded...");

function fetchJSONFile(path, callback) {
	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200) {
				console.log("fetchJSONFile: ", httpRequest.responseText)
				var data = JSON.parse(httpRequest.responseText);
				if (callback) callback(data);
			}
		}
	};

	httpRequest.open('GET', path);
	httpRequest.send();
}




function clearBLEDeviceList(peripheralList)
{
		var BLEDeviceListContainer = document.getElementsByClassName("ble-device-list-container")[0];
		var children = BLEDeviceListContainer.children;

		if(BLEDeviceListContainer === undefined && BLEDeviceListContainer === null)
				return false;


		for(var i = children.length - 1; i >= 0; --i)
		{
				var childNode = children[i];
				var removeNode = true;
				var deviceAddress = childNode.getAttribute("deviceAddress");

				for(var j = 0; j < peripheralList.length; ++j)
				{
						console.log("peripheralList[]: ", peripheralList[j].deviceAddress);
						console.log("deviceAddress: ", deviceAddress);

						if(peripheralList[j].deviceAddress === deviceAddress)
						{
								peripheralList.splice(j, 1);
								j--;
								removeNode = false;
								break;
						}
				}

				if(removeNode)
						childNode.parentNode.removeChild(childNode);
		}

		return true;
}




function addElementToBLEDeviceList(DOMElement)
{
		var BLEDeviceListContainer = document.getElementsByClassName("ble-device-list-container")[0];

		if(BLEDeviceListContainer === undefined && BLEDeviceListContainer === null)
				return false;

		BLEDeviceListContainer.appendChild(DOMElement);

		return true;
}




function BLEPeripheralBox(deviceAddress)
{
		var BLEPeripheralBoxTemplate = document.getElementsByClassName("ble-peripheral-box-template")[0];
		var BLEPeripheralDescriptionTemplate = document.getElementsByClassName("ble-peripheral-description-template")[0];
		var BLEPeripheralBox = BLEPeripheralBoxTemplate.cloneNode(true);

		BLEPeripheralBox.classList.remove("ble-peripheral-box-template");
		BLEPeripheralBox.classList.remove("hidden");


		var peripheralName 				     = BLEPeripheralBox.getElementsByClassName("name")[0];
		var peripheralShortDescription = BLEPeripheralBox.getElementsByClassName("short-description")[0];
		var peripheralImage 				 	 = BLEPeripheralBox.getElementsByClassName("ble-peripheral-img")[0];
		var peripheralDescriptionBox   = BLEPeripheralBox.getElementsByClassName("description-box")[0];
		var peripheralConnectButton    = BLEPeripheralBox.getElementsByClassName("connect-button")[0];

		BLEPeripheralBox.setAttribute("deviceAddress", deviceAddress);
		peripheralConnectButton.setAttribute("deviceAddress", deviceAddress);


		function setPeripheralName(name)
		{
				peripheralName.textContent = name;
		}

		function setPeripheralShortDescription(shortDescription)
		{
				peripheralShortDescription.textContent = shortDescription;
		}

		function addPeripheralDescription(description)
		{
			var	peripheralDescription = BLEPeripheralDescriptionTemplate.cloneNode(true);
			var peripheralDescriptionText = peripheralDescription.getElementsByClassName("description-text")[0];

			peripheralDescription.classList.remove("ble-peripheral-description-template");
			peripheralDescription.classList.remove("hidden");

			peripheralDescriptionText.textContent = description;

			peripheralDescriptionBox.appendChild(peripheralDescription);
		}

		function setImageSource(imageSource)
		{
				peripheralImage.setAttribute("src", imageSource);
		}

		function getDOMElement()
		{
				return BLEPeripheralBox;
		}



		this.setPeripheralName = setPeripheralName;
		this.setPeripheralShortDescription = setPeripheralShortDescription;
		this.addPeripheralDescription = addPeripheralDescription;
		this.setImageSource = setImageSource;
		this.getDOMElement = getDOMElement;
}






function generateBLEDeviceList() {

	var URL = "http://localhost:3000/device/list";


	 fetchJSONFile(URL, function(data) {

		 console.log(data);

		 var peripheralList = data;
		 clearBLEDeviceList(peripheralList);

		 for(var idx = 0; idx < peripheralList.length; ++idx) {
			 var peripheralBox = new BLEPeripheralBox(peripheralList[idx].deviceAddress);

			 peripheralBox.setPeripheralName(peripheralList[idx].deviceName);
			 peripheralBox.setPeripheralShortDescription("Address: " + peripheralList[idx].deviceAddress);
			 peripheralBox.addPeripheralDescription("Device RSSI: " + peripheralList[idx].rssi);
			 peripheralBox.addPeripheralDescription("Device UUID: " + peripheralList[idx].deviceUUID);

			 if(peripheralList[idx].deviceUUID !== "c856d580a9ab")
			 {
				 console.log("UUID: ", peripheralList[idx].deviceUUID, " deviceName: ", peripheralList[idx].deviceName);
				 peripheralBox.setImageSource("images/generic-peripheral.png");
			 }

			 addElementToBLEDeviceList(peripheralBox.getDOMElement());
		 }

	 });
}



function flip(flipDOMElement) {
	flipDOMElement.classList.toggle('flipped');
}



function connectToBLEDevice(connectButtonDOMElement) {
	console.log("Connect to BLE device...");

	var httpRequest = new XMLHttpRequest();
	var deviceAddress = connectButtonDOMElement.getAttribute('deviceAddress');

	console.log("deviceAddress: ", deviceAddress);

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			console.log("HTTP STATUS CODE: " + httpRequest.status);
			if (httpRequest.status === 200) {
				console.log("---------------------------------------");
				console.log("REDIRECT");
				console.log("---------------------------------------");
				console.log("responseURL: ");
				console.log(httpRequest.responseURL);
				window.location.replace(httpRequest.responseURL);
				return false;
			}
		}
	};


	httpRequest.open('GET', 'http://localhost:3000/device/connect/' + deviceAddress);
	httpRequest.send();

	return false;
}


var BLEDeviceListTimer = setInterval(generateBLEDeviceList, 500);

