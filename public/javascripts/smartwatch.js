var isBLEInitCompleated = false;
var isRegisteredStepCount = false;
var BLELEDState = 'on';

function toggleActiveStatus(DOMElement) {
	var activeStatus = DOMElement.getAttribute('active');

	if(activeStatus === 'on')
		DOMElement.setAttribute('active', 'off');
	else
		DOMElement.setAttribute('active', 'on');

}




function	initBLEDevice() {

	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200)
			{
				console.log("Discovered BLE Services");

				// ========================== DISCOVER CHARACTERISTICS =========================================
				var httpRequest2 = new XMLHttpRequest();

				httpRequest2.onreadystatechange = function() {
					if (httpRequest2.readyState === 4) {
						if (httpRequest2.status === 200)
						{
							console.log("Discovered BLE Characteristics");
							isBLEInitCompleated = true;
						}
					}
				};


				httpRequest2.open('GET', 'http://localhost:3000/device/discover/characteristic');
				httpRequest2.send();
				// ===============================================================================================
			}
		}
	};


	httpRequest.open('GET', 'http://localhost:3000/device/discover/service');
	httpRequest.send();
}


function	BLEDeviceLEDOn(callback)
{
	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200)
			{
				callback();
			}
		}
	};


	httpRequest.open('GET', 'http://localhost:3000/device/LED/on');
	httpRequest.send();
}



function	BLEDeviceLEDOff(callback)
{
	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {
			if (httpRequest.status === 200)
			{
				callback();
			}
		}
	};


	httpRequest.open('GET', 'http://localhost:3000/device/LED/off');
	httpRequest.send();
}



function toggleLEDState(DOMElement)
{
	if(isBLEInitCompleated === false)
		return;


	if(BLELEDState === 'on')
	{

		BLEDeviceLEDOff(function(){
				BLELEDState = 'off';
				DOMElement.getElementsByClassName("LED-button")[0].classList.add('off');
				DOMElement.getElementsByClassName("LED-button")[0].classList.remove('on');
		});

	}
	else
	{

		BLEDeviceLEDOn(function(){
				BLELEDState = 'on';
				DOMElement.getElementsByClassName("LED-button")[0].classList.add('on');
				DOMElement.getElementsByClassName("LED-button")[0].classList.remove('off');
		});

	}
}


function setImageUpload()
{
	var imageUploader = new ImageUploader();
}


function setClockView()
{
	var clock = new Clock();
}

function setFitnessScreen()
{
	var fitness = new Fitness(100);
}








function	BLEClockSync(year, month, day, wdu, hour, minute, second)
{
	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {

			if (httpRequest.status === 200)
			{

			}

		}
	};


	httpRequest.open('GET', 'http://localhost:3000/device/clocksync/' + year + '/'
																														 				+ month + '/'
																														 				+ day + '/'
																														 				+ wdu + '/'
																														 				+ hour + '/'
																														 				+ minute +'/'
																														 				+ second );
	httpRequest.send();
}



function	BLERegisterStepCount()
{
	if(isBLEInitCompleated !== true)
		return;

	if(isRegisteredStepCount === true)
		return;

	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4)
		{
				isRegisteredStepCount = true;
		}
	};

	httpRequest.open('GET', 'http://localhost:3000/device/stepCount/register')
	httpRequest.send();
}


function	BLEGetStepCount(trueFunc, falseFunc)
{

	//if(isRegisteredStepCount !== true)
	//	return;

	var httpRequest = new XMLHttpRequest();

	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === 4) {

			if (httpRequest.status === 200)
			{
				console.log("BLEGetStepCount(): this.responseText: ", this.responseText);
				trueFunc(this.responseText);
			}

			else
				falseFunc();
		}
	};

	httpRequest.open('GET', 'http://localhost:3000/device/stepCount')
	httpRequest.send();
}







function syncSmartWatchClock(syncButtonDOM)
{
	var currentTime = new Date();

	var year   = currentTime.getFullYear();
	var month  = currentTime.getMonth() + 1;
	var day    = currentTime.getDate();
	var wdu    = currentTime.getDay();
	var hour   = currentTime.getHours();
	var minute = currentTime.getMinutes();
	var second = currentTime.getSeconds();

	syncButtonDOM.classList.remove('spin');
	syncButtonDOM.classList.add('spin');

	BLEClockSync(year, month, day, wdu, hour, minute, second);

}





function ImageUploader()
{
	var smartWatchBg = document.querySelector('.smartwatch-bg');

	while(smartWatchBg.firstChild)
		smartWatchBg.removeChild(smartWatchBg.firstChild);


	var avatarUpload 			= document.createElement("DIV");
	var avatarEdit 	 			= document.createElement("DIV");
	var avatarPreview			= document.createElement("DIV");

	var uploadForm 				= document.createElement("FORM");

  var imageInput 				= document.createElement("INPUT");
	var imageInputLabel   = document.createElement("LABEL");

  var imageSubmit 	  	= document.createElement("INPUT");
	var imageSubmitLabel  = document.createElement("LABEL");


	var imagePreview 		= document.createElement("DIV");

	avatarUpload.classList.add('avatar-upload');
	avatarEdit.classList.add('avatar-edit');
	avatarPreview.classList.add('avatar-preview');

	uploadForm.id = "uploadForm"
	uploadForm.setAttribute('action', 'device/image');
	uploadForm.setAttribute('method', 'post');
	uploadForm.setAttribute('enctype', 'multipart/form-data');


	imageInput.id = "imageUpload";
	imageInput.setAttribute('type', 'file');
	imageInput.setAttribute('accept', '.png, .jpg, .jpeg');
	imageInput.setAttribute('name', 'image');

	imageInputLabel.setAttribute('for', 'imageUpload');



	imageSubmit.id = 'imageSubmit';
	imageSubmit.setAttribute('type', 'submit');
	imageSubmit.setAttribute('value', 'upload');

	imageSubmitLabel.setAttribute('for', 'imageSubmit');



	imagePreview.id = "imagePreview";
	imagePreview.setAttribute('style', 'background-image: url(images/bg.jpg);');


	avatarUpload.appendChild(avatarEdit);
	avatarUpload.appendChild(avatarPreview);

	avatarEdit.appendChild(uploadForm);
	uploadForm.appendChild(imageInput);
	uploadForm.appendChild(imageInputLabel);
	uploadForm.appendChild(imageSubmit);
	uploadForm.appendChild(imageSubmitLabel);

	avatarPreview.appendChild(imagePreview);


	smartWatchBg.appendChild(avatarUpload);


	function setBackgroundImageFromURL(input) {
		console.log("setBackgroundImageFromURL(): called");

		if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
	}


	$("#imageUpload").change(function() {
    setBackgroundImageFromURL(this);
	});


  $("#uploadForm").submit(function (event) {
   	event.preventDefault();

   	var formData = new FormData(this);
   	console.log(formData);

   	var formDataSerialized = $(this).serialize();
   	console.log(formDataSerialized);

   	$.ajax({
   		url: 'device/image',
   		type: 'POST',
   		data: formData,
   		async: false,
   		cache: false,
   		contentType: false,
   		processData: false,
   		success: function (returndata) {
   			alert("Successful upload");
   		},
   		error: function () {
   			alert("error in ajax form submission");
   		}
   	});

		return false;
	});

}




function Clock()
{
	var smartWatchBg = document.querySelector('.smartwatch-bg');

	while(smartWatchBg.firstChild)
		smartWatchBg.removeChild(smartWatchBg.firstChild);


	var clock 		 = document.createElement("DIV");
	var hourHand 	 = document.createElement("SPAN");
 	var minuteHand = document.createElement("SPAN");
  var secondHand = document.createElement("SPAN");
	var clockDot   = document.createElement("SPAN");

	clock.classList.add('clock1');
	hourHand.classList.add('hour');
	minuteHand.classList.add('minute');
	secondHand.classList.add('second');
	clockDot.classList.add('dot');

	clock.appendChild(hourHand);
	clock.appendChild(minuteHand);
	clock.appendChild(secondHand);
	clock.appendChild(clockDot);

	smartWatchBg.appendChild(clock);


	function update()
	{
  		var date = new Date();

  		var hours = ((date.getHours() + 11) % 12 + 1);
  		var minutes = date.getMinutes();
  		var seconds = date.getSeconds();

  		var hour = hours * 30;
  		var minute = minutes * 6;
  		var second = seconds * 6;

			hourHand.style.transform 	 = 'rotate(' + hour + 'deg)';
			minuteHand.style.transform = 'rotate(' + minute + 'deg)';
			secondHand.style.transform = 'rotate(' + second + 'deg)';
	}


	console.log("Clock(): constructor");
	update();
	setInterval(update, 1000);
}





function Fitness(initStepCount)
{
	var smartWatchBg = document.querySelector('.smartwatch-bg');

	while(smartWatchBg.firstChild)
		smartWatchBg.removeChild(smartWatchBg.firstChild);

	/*
					<div class="fitness-bg">
						<div class="fitness">
						</div>
						<div class="fitness-info">
							100
						</div>
					</div>
	*/

	var fitnessBg 	 = document.createElement("DIV");
	var fitnessIcon  = document.createElement("DIV");
 	var fitnessInfo  = document.createElement("DIV");

	fitnessBg.classList.add('fitness-bg');
	fitnessIcon.classList.add('fitness-icon');
	fitnessInfo.classList.add('fitness-info');

	fitnessInfo.textContent = "Loading...";

	fitnessBg.appendChild(fitnessIcon);
	fitnessBg.appendChild(fitnessInfo);

	smartWatchBg.appendChild(fitnessBg);


	var timer = setInterval(function() {
		console.log("Fitness(): TIMER FUNCITON CALLED");
		BLERegisterStepCount();
		BLEGetStepCount(updateStepCount, function() {});
	}, 1000);


	function updateStepCount(stepCount)
	{
		console.log("updateStepCount(): stepCount => ", stepCount);
		fitnessInfo.textContent = stepCount;
	}


	function destructor()
	{
		clearInterval(timer);
	}

}










var clock = new Clock();
//var imageUploader = new ImageUploader();
//var fitness = new Fitness(10);
initBLEDevice();

