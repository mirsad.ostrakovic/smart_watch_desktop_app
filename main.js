'use strict';

const {app, BrowserWindow} = require('electron');
const path = require('path');
//const noble = require('noble');

const defaultProps = {
	width: 800,
	height: 600,
	show: false,
	titleBarStyle: 'hidden'
}


class Window extends BrowserWindow {

	// Window constructor method
	constructor({file, ...windowSettings})
	{
		// Call BrowserWindow contructor wirh these props
		super({...defaultProps, ...windowSettings});

		// Create file path
		const filePath = path.join('src', file);

		// Load the HTML into the window
		this.loadFile(filePath);

		// Open devtools, only for development
		this.webContents.openDevTools();

		// Show window when all rendering is compleated
		this.once('ready-to-show', () => { this.show(); });
	}


	// Send message to window with IPC communication
	sendMessage(messageID, content)
	{
		this.webContents.send(messageID, content);
	}

}



function main() {
	let mainWindow = new Window({
		file: 'connect.html'
	});

	mainWindow.webContent.on('did-finish-load', () => {
		sendMessage('TEST_MSG', "Hello there!");
	});
}




app.on('ready', main);

app.on('window-all-closed', function() {
	app.quit();
});
